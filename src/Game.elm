{- Eric Y Chang
   CS 22300 Final Project
   Snake game
   
   Run the command:
   > elm make src/Game.elm --output src/Game.js
   then open src/index.html
-}

port module Game exposing (main)

import Browser
import Browser.Events
import Json.Decode as Decode
import Html exposing (Html)
import Html.Attributes as Attr
import Random exposing (Generator)
import Time

import Grid exposing (..)
import Snake exposing (..)
import RedBlackTree exposing (..)


{------------ MAIN ------------}

main : Program Flags Model Msg
main =
    Browser.element
        { init = initModel
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

-- Ports to connect with index.html
port saveScore : Int -> Cmd msg
port loadScoreRequest : () -> Cmd msg
port loadScoreReceive : (Int -> msg) -> Sub msg

{------------ END MAIN ------------}


{------------ MODEL ------------} 

gridWidth : Int
gridWidth = 
    21

gridHeight : Int
gridHeight =
    15

type GameState
    = Active -- gameplay state
    | GameOver
    | Paused
    | PreGame

type alias Model =
    { snake : Snake
    , apple : Cell
    , grid : Grid
    , score : Int
    , highScore : Int
    , status : GameState
    }

type alias Flags =
    ()

initModel : Flags -> (Model, Cmd Msg)
initModel () =
    let
        model =
            { snake = init 0 North -- placeholder, will be replaced by random
            , apple = 0 -- placeholder, will be replaced by random
            , grid = makeGrid gridWidth gridHeight
            , score = 0
            , highScore = 0 -- placeholder, will be replaced by JS interop ports
            , status = PreGame
            }
    in
        (model, loadScoreRequest ())

{------------ MODEL END ------------}


{------------ UPDATE ------------}

type Msg
    = CreateNewSnake (Int, Direction)
    | ChangeSnakeDirection Direction
    | GenerateApple Int
    | LoadScoreReceive Int
    | OtherKeyDown
    | StartGame
    | Tick -- time tick

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        CreateNewSnake (x, d) -> -- need to generate new apple after snake creation
            ({ model | snake = init x d }, randomApple) 
        ChangeSnakeDirection d ->
            case (model.status, changeDirection model.snake d) of
                (Active, Just newsnake) ->
                    ({ model | snake = newsnake }, Cmd.none)
                _ -> -- either game not active or bad direction change
                    (model, Cmd.none)
        GenerateApple x -> -- ensures apple isn't placed where snake already exists
            case member x (getTree model.snake) of
                True -> 
                    (model, randomApple)
                _ ->
                    ({ model | apple = x}, Cmd.none)
        LoadScoreReceive score ->
            ({ model | highScore = score }, randomSnake)
        OtherKeyDown ->
            (model, Cmd.none)
        StartGame ->
            case model.status of
                Active -> 
                    ({ model | status = Paused }, Cmd.none)
                GameOver ->
                    initModel ()
                _ -> 
                    ({ model | status = Active }, Cmd.none)
        Tick -> 
            case model.status of
                Active ->
                    tickEvent model
                _ ->
                    (model, Cmd.none)

-- Generator for a random grid spot.
randomGridSpace : Random.Generator Int
randomGridSpace =
    let
        gridSize = gridWidth * gridHeight
    in
        Random.int 0 (gridSize-1)

-- Generator for a snake placed in a random
-- grid spot and facing a random direction.
randomSnake : Cmd Msg
randomSnake =
    let
        randomDirection : Random.Generator Direction
        randomDirection =
            Random.uniform North [ South, East, West ]
    in 
        Random.pair randomGridSpace randomDirection
        |> Random.generate CreateNewSnake

-- Generator for an apple placed in a random grid spot.
randomApple : Cmd Msg
randomApple =
    Random.generate GenerateApple randomGridSpace

-- Handle movement of snake after a time tick.
tickEvent : Model -> (Model, Cmd Msg)
tickEvent model =
    case move model.snake model.grid of
        Nothing -> -- ran into self/wall -> game over
            if model.highScore < model.score then -- new high score
                ( { model | highScore = model.score,
                           status = GameOver } 
                , saveScore model.score)
            else
                ({ model | status = GameOver }, Cmd.none)
        Just (head, _, snake) ->
            if head == model.apple then -- apple eaten
                case grow model.snake model.grid of
                    Just (_, newsnake) ->
                        ({ model | snake = newsnake
                                 , score = (model.score + 1)}
                         , randomApple) -- generate new apple
                    _ ->
                        Debug.todo "impossible"
            else -- search for apple continues
                ({ model | snake = snake }, Cmd.none)

{------------ UPDATE END ------------}


{------------ VIEW ------------}

darkcolor : String
darkcolor =
    "#2b331a"

lightcolor : String 
lightcolor =
    "#9bba5a"

cellsize : String
cellsize =
    "30px"

defaultStyles : List (Html.Attribute msg)
defaultStyles =
    [ Attr.style "color" darkcolor
    , Attr.style "font-family" "Lucida Console, Courier New, monospace"
    , Attr.style "font-size" "20px"
    , Attr.style "text-align" "center"
    ]

nonGameStyles : List (Html.Attribute msg)
nonGameStyles = 
    let 
        newStyles =
            [ Attr.style "background-color" lightcolor
            , Attr.style "width" "50vw"
            , Attr.style "height" "40vh"
            , Attr.style "padding-top" "25vh"
            , Attr.style "position" "fixed"
            , Attr.style "top" "50%"
            , Attr.style "left" "50%"
            , Attr.style "transform" "translate(-50%, -50%)"
            ]
    in
        defaultStyles ++ newStyles

view : Model -> Html Msg
view model =
    case model.status of 
        Active -> 
            activeGameScreen model
        GameOver ->
            gameOverScreen model
        Paused ->
            pausedScreen model
        PreGame ->
            startScreen model
        
-- HTML shown on Active status
activeGameScreen : Model -> Html Msg
activeGameScreen model =
    let
        scoreText = "Current score: " ++ (String.fromInt model.score)
        scoreElement = Html.p defaultStyles [Html.text scoreText]
        tableElement = buildTable model
    in
        Html.div [] [scoreElement, tableElement]

-- helper for activeGameScreen
buildTable : Model -> Html Msg
buildTable model =
    let
        helper : List (Html Msg) -> Int -> List (Html Msg)
        helper acc n =
            if n < 0 then
                acc
            else
                helper ((buildRow model n) :: acc) (n-1)
        styles =
            [ Attr.style "background-color" lightcolor
            , Attr.style "color" darkcolor
            , Attr.style "border" ("solid 5px" ++ darkcolor)
            , Attr.style "position" "fixed"
            , Attr.style "top" "50%"
            , Attr.style "left" "50%"
            , Attr.style "transform" "translate(-50%, -50%)"
            ]
    in
        helper [] (height model.grid - 1)
        |> Html.table styles
-- I figure a table would be easiest for rendering, 
-- which is why I went with that.
-- Styling based on https://playsnake.org/

-- helper for buildTable
buildRow : Model -> Int -> Html Msg
buildRow model row =
    let 
        tree = model.snake |> getTree
        styles =
            [ Attr.style "width" cellsize
            , Attr.style "height" cellsize
            ]
        occupiedStyles = (Attr.style "background-color" darkcolor) :: styles
        snakeStyles = (Attr.style "border-radius" "5px") :: occupiedStyles
        appleStyles = (Attr.style "border-radius" cellsize) :: occupiedStyles
        helper : List (Html Msg) -> Int -> List (Html Msg)
        helper acc n =
            if n < 0 then  
                acc
            else
                let
                    -- color in cell if the snake/apple is occupying this cell
                    thisCell = n + (row * width model.grid)
                    realStyles = if member thisCell tree then 
                                     snakeStyles
                                 else if model.apple == thisCell then
                                     appleStyles
                                 else
                                     styles
                in
                    helper ((Html.td realStyles []) :: acc) (n-1)
    in
        helper [] (width model.grid - 1)
        |> Html.tr []

-- HTML shown on GameOver status
gameOverScreen : Model -> Html Msg
gameOverScreen model =
    let 
        scoreText = "Final score: " ++ (String.fromInt model.score)
        highScoreText = if model.highScore == model.score then
                            "New high score!"
                        else
                            "Current high score: " ++ (String.fromInt model.highScore)
        textElement1 = Html.p [] [Html.text scoreText]
        textElement2 = Html.p [] [Html.text highScoreText]
        textElement3 = Html.p [] [Html.text "Press [space]"]
        allText = [ textElement1
                  , textElement2
                  , textElement3
                  ]
    in
        Html.div nonGameStyles allText

-- HTML shown on Paused status
pausedScreen : Model -> Html Msg
pausedScreen model =
    let
        text = Html.text "Press [space] to resume"
    in
        Html.div nonGameStyles [text]

-- HTML shown on PreGame status
startScreen : Model -> Html Msg
startScreen model =
    let
        highScoreText = "Current high score: " ++ (String.fromInt model.highScore)
        text = "Press [space] to begin"
        textElement1 = Html.p [] [Html.text highScoreText]
        textElement2 = Html.p [] [Html.text text]
        allText = 
            [ textElement1
            , textElement2]
    in
        Html.div nonGameStyles allText

{------------ VIEW END ------------}


{------------ SUBSCRIPTIONS ------------}

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onKeyDown <| Decode.map keyResponse keyDecoder
        , loadScoreReceive LoadScoreReceive
        , Time.every 170 <| always Tick
        ]

keyResponse : String -> Msg
keyResponse key =
    if key == " " then
        StartGame
    else if key == "ArrowDown" || String.toUpper key == "S" then
        ChangeSnakeDirection South
    else if key == "ArrowLeft" || String.toUpper key == "A" then 
        ChangeSnakeDirection West
    else if key == "ArrowRight" || String.toUpper key == "D" then
        ChangeSnakeDirection East
    else if key == "ArrowUp" || String.toUpper key == "W" then
        ChangeSnakeDirection North
    else
        OtherKeyDown
-- https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values

keyDecoder : Decode.Decoder String
keyDecoder =
    Decode.field "key" Decode.string

{------------ SUBSCRIPTIONS END ------------}