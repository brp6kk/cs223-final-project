module RedBlackTreeTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import RedBlackTree exposing (..)

tree1 : Tree Int
tree1 = 
    List.range 1 5
    |> List.foldl insert empty 

tree2 : Tree Int
tree2 =
    List.range 1 10
    |> List.foldl insert empty

tree3 : Tree Int
tree3 = 
    empty |> insert 4 |> insert 11 
    |> insert 9 |> insert 0 |> insert 2 
    |> insert 111 |> insert 65 |> insert 1
    |> insert 2 |> insert 8 |> insert 20
    |> insert 7 |> insert 12 |> insert 99
    |> insert 88 |> insert 42 |> insert 31
    |> insert 98 |> insert 77 |> insert 12

tree4 : Tree Int
tree4 = 
    List.range 0 100
    |> List.foldl insert empty


suite : Test
suite = 
    describe "Red-black trees"
        [ describe "Insert"
            [ test "insert 1 2 3 4 5" <|
                \_ -> 
                    redBlackTree tree1
                    |> Expect.true "Expected proper RBT"
            , test "insert 5 4 3 2 1" <|
                \_ ->
                    let
                        tree = List.range 1 5 
                               |> List.reverse 
                               |> List.foldl insert empty
                    in
                        redBlackTree tree 
                        |> Expect.true "Expected proper RBT"
            , test "insert lots of values in tree" <|
                \_ ->
                    redBlackTree tree4
                    |> Expect.true "Expected proper RBT"
            ]
        , describe "Member"
            [ test "member valid (insert only)" <|
                \_ ->
                    member 3 tree2
                    |> Expect.true "3 exists in tree"
            , test "member invalid (insert only)" <|
                \_ ->
                    member 100 tree2
                    |> Expect.false "100 does not exist in tree"
            ]
        , describe "Remove"
            [ test "try removal from tree 1 2 3 4 5" <| 
                \_ -> 
                    let
                        fiveTrees = List.repeat 5 tree1 
                        removedTrees = List.map2 remove (List.range 1 5) fiveTrees
                        notMembers = List.map2 member (List.range 1 5) removedTrees 
                                   |> List.all not
                        rbts = List.all redBlackTree removedTrees

                    in
                        notMembers && rbts
                        |> Expect.true "Expected removal of values"
            , test "try removal of multiple elements from a tree" <|
                \_ ->
                    let
                        removedTree = tree3 |> remove 4 |> remove 65 |> remove 8
                        threeTrees = List.repeat 3 removedTree
                        notMembers = List.map2 member [4, 65, 8] threeTrees
                                     |> List.all not
                    in
                        redBlackTree removedTree && notMembers
                        |> Expect.true "Expected removal of all values"
            , test "try removing nonexistant element from a tree" <|
                \_ ->
                    let
                        removedTree = remove 100 tree2
                    in
                        redBlackTree removedTree && not (member 100 removedTree)
                        |> Expect.true "Removed value that does not exist"
            , test "removal creates an empty tree" <|
                \_ ->
                    let 
                        tree = empty |> insert 1 |> insert 2
                               |> remove 1 |> remove 2 |> remove 44
                    in
                        empty == tree
                        |> Expect.true "Tree should be empty"
            , Test.fuzzWith { runs = 10 } (Fuzz.intRange 0 100) 
                "removing random elements from big tree still produces RBT" <|
                \n -> 
                    let
                        removedTree = remove n tree4
                    in
                        redBlackTree removedTree && not (member n removedTree)
                        |> Expect.true "Didn't remove random value"
            , Test.fuzzWith { runs = 10 } (Fuzz.intRange 0 100) 
                "removing random elements from arbitrary tree still produces RBT" <|
                \n ->
                    let
                        removedTree = remove n tree3
                    in
                        redBlackTree removedTree && not (member n removedTree)
                        |> Expect.true "Didn't remove random value"
            ]
        ]