module SnakeTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import Snake exposing (..)
import Grid exposing (..)
import RedBlackTree as T
import Queue as Q

grid1 : Grid
grid1 = 
    makeGrid 10 10

cell1 : Cell
cell1 = 
    case makeCell grid1 45 of 
        Just c -> 
            c
        _ ->
            Debug.todo "impossible"

cell2 : Cell
cell2 = 
    case makeCell grid1 5 of 
        Just c -> 
            c
        _ ->
            Debug.todo "impossible"

snake1 : Snake
snake1 =
    init cell1 North

snake2 : Snake
snake2 =
    init cell1 South

snake3 : Snake
snake3 =
    init cell1 East

snake4 : Snake
snake4 =
    init cell1 West

getSnakeGrow : Maybe (Cell, Snake) -> Snake
getSnakeGrow m =
    case m of
        Just (_, s) ->
            s
        _ -> 
            Debug.todo "whoops"

growHelper : Snake -> Snake
growHelper s =
    grow s grid1 |> getSnakeGrow

getSnakeMove : Maybe (Cell, Cell, Snake) -> Snake
getSnakeMove m =
    case m of
        Just (_, _, s) ->
            s
        _ -> 
            Debug.todo "whoops"

moveHelper : Snake -> Snake
moveHelper s =
    move s grid1 |> getSnakeMove

getSnakeCD : Maybe Snake -> Snake
getSnakeCD s = 
    case s of
        Just val ->
            val 
        Nothing ->
            Debug.todo "whoops"

changeDirectionHelper : Direction -> Snake -> Snake
changeDirectionHelper d s =
    changeDirection s d |> getSnakeCD

suite : Test
suite =
    describe "Snake"
        [ describe "init"
            [ test "init snake" <|
                \_ -> 
                    let
                        snake = init cell1 North
                        expectedTree = T.empty |> T.insert 45
                        expectedQueueVal = Q.empty |> Q.enqueue 45 |> Q.peek
                        actualTree = getTree snake
                        actualQueueVal = getQueue snake |> Q.peek
                        actualDirection = getDirection snake
                        actualHead = getHead snake
                    in
                        Expect.true "bad snake init" <| 
                            expectedTree == actualTree && 
                            expectedQueueVal == actualQueueVal && 
                            North == actualDirection &&
                            cell1 == actualHead
            ]
        , describe "grow"
            [ test "valid grow" <|
                \_ ->
                    let
                        expectedTree = T.empty |> T.insert 45 |> T.insert 35
                    in
                        case grow (init cell1 North) grid1 of
                            Just (c, Snake {bodyTree, bodyQueue, direction, head}) ->
                                Expect.true "bad snake grow" <|
                                    c == 35 &&
                                    bodyTree == expectedTree && 
                                    direction == North &&
                                    head == 35
                            _ -> 
                                Expect.fail "didn't grow"
            , test "bad grow" <|
                \_ -> 
                    Expect.equal (grow (init cell2 North) grid1) Nothing             
            ]
        , describe "move"
            [ test "valid move" <|
                \_ ->
                    let 
                        expectedTree = T.empty |> T.insert 35
                        expectedQueueVal = Q.empty |> Q.enqueue 35 |> Q.peek
                    in
                        case move (init cell1 North) grid1 of
                            Just (c1, c2, Snake {bodyTree, bodyQueue, direction, head}) ->
                                Expect.true "bad snake move" <|
                                    c1 == 35 &&
                                    c2 == 45 &&
                                    bodyTree == expectedTree && 
                                    expectedQueueVal == Q.peek bodyQueue &&
                                    head == 35
                            _ ->
                                Expect.fail "didn't move"
            , test "bad move" <|
                \_ -> 
                    Expect.equal (move (init cell2 North) grid1) Nothing
            ]
        , describe "changeDirection"
            [ test "valid change" <|
                \_ -> 
                    case changeDirection snake1 East of
                        Just (Snake {direction}) ->
                            Expect.equal direction East
                        _ ->
                            Expect.fail "bad direction"
            , test "valid change again" <|
                \_ ->
                    case changeDirection snake3 North of
                        Just (Snake {direction}) ->
                            Expect.equal direction North
                        _ ->
                            Expect.fail "bad direction"
            , test "multiple valid changes" <|
                \_ ->
                    let
                        result = changeDirectionHelper East snake1
                                 |> changeDirectionHelper South
                    in
                        case result of
                            Snake {direction} ->
                                Expect.equal direction South
            , test "bad direction change north -> south" <|
                \_ ->
                    Expect.equal (changeDirection snake1 South) Nothing
            , test "bad direction change south -> north" <|
                \_ ->
                    Expect.equal (changeDirection snake2 North) Nothing
            , test "bad direction change east -> west" <|
                \_ ->
                    Expect.equal (changeDirection snake3 West) Nothing
            , test "bad direction change west -> east" <|
                \_ ->
                    Expect.equal (changeDirection snake4 East) Nothing
            , test "bad direction change after some valid changes" <|
                \_ ->
                    let
                        result = changeDirectionHelper East snake1
                                 |> changeDirectionHelper South
                                 |> changeDirectionHelper East
                    in
                        Expect.equal (changeDirection result West) Nothing
            ]
        , describe "combo"
            [ test "valid combo of changes" <|
                \_ ->
                    let
                        result = growHelper snake1
                                 |> growHelper
                                 |> changeDirectionHelper West
                                 |> growHelper
                                 |> growHelper
                                 |> growHelper
                                 |> moveHelper
                                 |> changeDirectionHelper South
                                 |> moveHelper
                        expectedTree = T.empty |> T.insert 45 |> T.insert 35 |> T.insert 25 |> T.insert 24 |> T.insert 23 |> T.insert 22 |> T.insert 21 |> T.remove 45 |> T.insert 31 |> T.remove 35
                        expectedQueueVal = Just 25
                        actualTree = getTree result
                        actualQueueVal = getQueue result |> Q.peek
                        actualDirection = getDirection result
                        actualHead = getHead result
                    in
                        Expect.true "bad snake combo" <|
                            expectedTree == actualTree &&
                            expectedQueueVal == actualQueueVal &&
                            South == actualDirection &&
                            31 == actualHead
            , test "smack into grid border" <|
                \_ ->
                    let
                        precollision = growHelper snake1
                                       |> growHelper
                                       |> moveHelper
                                       |> moveHelper
                        postcollision = move precollision grid1
                    in
                        Expect.equal postcollision Nothing
            , test "smack into itself" <|
                \_ ->
                    let
                        precollision = growHelper snake1
                                       |> changeDirectionHelper East
                                       |> growHelper
                                       |> changeDirectionHelper South
                                       |> growHelper
                                       |> changeDirectionHelper West
                        postcollision = grow precollision grid1
                    in
                        Expect.equal postcollision Nothing
            ]
        ]