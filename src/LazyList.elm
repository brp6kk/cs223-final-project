{- Heavily based on the files LazyList.elm & ThunkList.elm,
   but with some trivial naming/spacing changes
   + the removal of the misbehaving reverse.
   https://www.classes.cs.uchicago.edu/archive/2021/winter/22300-1/lectures/LazyLists/index.html
   Note that I only use lazy lists in my queue implementation.
-}
module LazyList exposing (..)

type Thunk a =
    Thunk (() -> a)

lazy : (() -> a) -> Thunk a
lazy = 
    Thunk

force : Thunk a -> a
force (Thunk f) = 
    f ()

type alias LazyList a =
    Thunk (LazyListCell a)

type LazyListCell a
    = Nil
    | Cons a (LazyList a)

range : Int -> Int -> LazyList Int
range i j =
    lazy <| \() ->
        if i > j then
            Nil
        else
            range (i+1) j
            |> Cons i

toList : LazyList a -> List a
toList =
    let
        helper acc lxs =
            case force lxs of
                Nil ->
                    acc
                Cons x lxsRest ->
                    helper (x::acc) lxsRest
    in
        List.reverse << helper []

infinite : a -> LazyList a
infinite a =
    lazy <| \() -> 
        infinite a
        |> Cons a

take : Int -> LazyList a -> LazyList a
take n llist =
    lazy <| \() ->
        if n <= 0 then
            Nil
        else
            case force llist of
                Nil ->
                    Nil
                Cons x llistRest ->
                    take (n-1) llistRest
                    |> Cons x

drop : Int -> LazyList a -> LazyList a
drop n llist =
    let
        helper k lxs =
            if k <= 0 then
                force lxs
            else
                case force lxs of
                    Nil ->
                        Nil
                    Cons _ lxsRest ->
                        helper (k-1) lxsRest
    in
        lazy <| \() -> 
            helper n llist

append : LazyList a -> LazyList a -> LazyList a
append lxs lys =
    lazy <| \() ->
        case force lxs of
            Nil ->
                force lys
            Cons x lxsRest ->
                append lxsRest lys
                |> Cons x