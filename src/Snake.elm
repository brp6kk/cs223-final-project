module Snake exposing (..)

import RedBlackTree as T
import Queue as Q
import Grid exposing (..)

type Snake 
    = Snake { bodyTree : T.Tree Cell
            , bodyQueue : Q.Queue Cell
            , direction : Direction
            , head : Cell
            }

-- Create a snake of length 1 moving in the provided direction.
init : Cell -> Direction -> Snake
init c d =
    let
        tree = T.empty |> T.insert c
        queue = Q.empty |> Q.enqueue c
    in
        Snake { bodyTree = tree
              , bodyQueue = queue
              , direction = d
              , head = c
              }

-- Increase the length of the snake by 1,
-- returning both the cell into which it grew
-- and the new snake;
-- or returns nothing if doing so moves
-- the snake off of the grid or causes it
-- to collide with itself.
grow : Snake -> Grid -> Maybe (Cell, Snake)
grow (Snake {bodyTree, bodyQueue, direction, head}) g = 
    case nextCell g head direction of
        Just newCell ->
            if T.member newCell bodyTree then
                Nothing -- collided with itself
            else -- valid movement
                let
                    newTree = T.insert newCell bodyTree
                    newQueue = Q.enqueue newCell bodyQueue
                in
                    (newCell, Snake { bodyTree = newTree
                                    , bodyQueue = newQueue
                                    , direction = direction
                                    , head = newCell
                    })
                    |> Just
        _ -> -- moved off of grid
            Nothing
    
-- Move the snake, returning 
-- 1. the cell into which it moved
-- 2. the cell which it moved from
-- 3. the updated snake;
-- or returns nothing
-- if doing so moves the snake off of the grid
-- or causes it to collide with itself.
move : Snake -> Grid -> Maybe (Cell, Cell, Snake)
move s g =
    case grow s g of 
        Just (cell, Snake {bodyTree, bodyQueue, direction, head}) -> -- successful grow
            let
                (mback, newq) = Q.dequeue bodyQueue
            in
                case mback of 
                    Just back ->
                        (cell, back, Snake {bodyTree = T.remove back bodyTree
                                           , bodyQueue = newq
                                           , direction = direction
                                           , head = head
                        }) 
                        |> Just
                    _ -> -- shouldn't happen in theory
                        Nothing
        _ -> -- either moved off grid or collided with itself
            Nothing

-- Changes the direction of snake movement,
-- or returns nothing if such a direction
-- change is impossible
changeDirection : Snake -> Direction -> Maybe Snake
changeDirection (Snake {bodyTree, bodyQueue, direction, head}) newD =
    if ((newD == East && direction == West) ||
        (newD == West && direction == East) ||
        (newD == North && direction == South) ||
        (newD == South && direction == North)
       ) then
        Nothing
    else
        Snake { bodyTree = bodyTree
              , bodyQueue = bodyQueue
              , direction = newD
              , head = head
              }
        |> Just

getTree : Snake -> T.Tree Cell
getTree (Snake {bodyTree}) =
    bodyTree

getQueue : Snake -> Q.Queue Cell
getQueue (Snake {bodyQueue}) =
    bodyQueue

getDirection : Snake -> Direction
getDirection (Snake {direction}) =
    direction

getHead : Snake -> Cell
getHead (Snake {head}) =
    head