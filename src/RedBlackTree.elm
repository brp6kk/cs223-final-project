{- Heavily based on the file RedBlackDelete.elm,
   but with some edits to get expected behavior
   (and also some spacing edits to make working through this
   easier on my end).
   https://www.classes.cs.uchicago.edu/archive/2021/winter/22300-1/lectures/RedBlackDelete/index.html
-}
module RedBlackTree exposing 
    (Tree, empty, member, insert, remove, redBlackTree)
-- redBlackTree is exposed for testing purposes only

type Color
    = BB -- "double black"
    | B
    | R
    | RR -- "double red"

type Tree a
    = E  -- "empty"
    | BE -- "black empty"
    | T Color (Tree a) a (Tree a)

incr : Color -> Color
incr c =
    case c of
        BB -> Debug.todo "incr BB"
        B  -> BB
        R  -> B
        RR -> R

decr : Color -> Color
decr c =
    case c of
        BB -> B
        B  -> R
        R  -> RR
        RR -> Debug.todo "decr RR"

-- Return empty tree.
empty : Tree a
empty = 
    E

-- Determine if value is in a red-black tree.
member : comparable -> Tree comparable -> Bool
member x t = 
    case t of
        E -> 
            False
        T _ l y r ->
            if x == y then 
                True
            else if x < y then 
                member x l
            else -- x > y
                member x r
        BE -> 
            Debug.todo "member BE"

-- Insert an element into tree.
insert : comparable -> Tree comparable -> Tree comparable
insert x t =
    case ins x t of
        T _ l y r -> 
            T B l y r
        _         -> 
            Debug.todo "insert E"

-- helper for insert
ins : comparable -> Tree comparable -> Tree comparable
ins x t =
    case t of
        E -> 
            T R E x E
        T c l y r ->
            if x == y then 
                t
            else if x < y then 
                balance c (ins x l) y r
            else -- x > y
                balance c l y (ins x r)
        BE -> 
            Debug.todo "ins BE"

type alias Balance comparable =
    Color -> Tree comparable -> comparable -> Tree comparable -> Tree comparable

type alias BalanceMaybe comparable =
    Color -> Tree comparable -> comparable -> Tree comparable -> Maybe (Tree comparable)

-- balance a potentially invariant-violating tree
balance : Balance comparable
balance c l val r =
    balance_B_R_R c l val r
    |> orMaybe (balance_BB_R_R c l val r)
    |> orMaybe (balance_BB_RR c l val r)
    |> Maybe.withDefault (T c l val r)
    |> bubble_BE_and_BB -- EDIT added line

balance_B_R_R : BalanceMaybe comparable
balance_B_R_R color l val r =
    case (color, (l, val, r)) of
        (B, (T R (T R a x b) y c, z, d)) -> 
            T R (T B a x b) y (T B c z d) |> Just
        (B, (T R a x (T R b y c), z, d)) -> 
            T R (T B a x b) y (T B c z d) |> Just
        (B, (a, x, T R (T R b y c) z d)) -> 
            T R (T B a x b) y (T B c z d) |> Just
        (B, (a, x, T R b y (T R c z d))) ->
            T R (T B a x b) y (T B c z d) |> Just
        _ -> 
            Nothing

balance_BB_R_R : BalanceMaybe comparable
balance_BB_R_R color l val r =
    case (color, (l, val, r)) of
        (BB, (T R (T R a x b) y c, z, d)) ->
            T B (T B a x b) y (T B c z d) |> Just
        (BB, (T R a x (T R b y c), z, d)) ->
            T B (T B a x b) y (T B c z d) |> Just
        (BB, (a, x, T R (T R b y c) z d)) ->
            T B (T B a x b) y (T B c z d) |> Just
        (BB, (a, x, T R b y (T R c z d))) ->
            T B (T B a x b) y (T B c z d) |> Just
        _ -> 
            Nothing

balance_BB_RR : BalanceMaybe comparable
balance_BB_RR color l val r =
    case (color, (l, val, r)) of
        (BB, (T RR (T B a w b) x (T B c y d), z, e)) ->
            T B (balance B (T R a w b) x c) y (T B d z e) |> Just
        (BB, (a, w, T RR (T B b x c) y (T B d z e))) ->
            T B (T B a w b) x (balance B c y (T R d z e)) |> Just
        _ -> 
            Nothing

bubble_BE_and_BB : Tree comparable -> Tree comparable
bubble_BE_and_BB t =
    case t of
        -- cases 1a, 2a, 3a
        T c1 (T c2 a x b) y BE ->
            case (c1, c2) of
                (R, B) -> 
                    balance (incr c1) (T (decr c2) a x b) y E
                (B, B) -> 
                    balance (incr c1) (T (decr c2) a x b) y E
                (B, R) -> 
                    balance (incr c1) (T (decr c2) a x b) y E
                _ -> 
                    t
        -- cases 1b, 2b, 3b
        T c1 BE y (T c3 c z d) ->
            case (c1, c3) of
                (R, B) -> 
                    balance (incr c1) E y (T (decr c3) c z d)
                (B, B) -> 
                    balance (incr c1) E y (T (decr c3) c z d)
                (B, R) -> 
                    balance (incr c1) E y (T (decr c3) c z d)
                _ -> 
                    t
        -- cases 1a', 1b', 2a', 2b', 3a', 3b'
        T c1 (T c2 a x b) y (T c3 c z d) ->
            case (c1, c2, c3) of
                (R, B, BB) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                (R, BB, B) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                (B, B, BB) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                (B, BB, B) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                (B, R, BB) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                (B, BB, R) -> 
                    balance (incr c1) (T (decr c2) a x b) y (T (decr c3) c z d)
                _ -> 
                    t
        _ -> 
            t

-- Remove an element from a tree.
remove : comparable -> Tree comparable -> Tree comparable
remove x t =
    case rem x t of
        T _ l y r -> 
            T B l y r
        _ -> 
            E -- EDIT removing element from singleton tree returns empty
              -- this is more of a personal preference for how I think remove should behave

-- helper for remove
rem : comparable -> Tree comparable -> Tree comparable
rem n t =
    case t of
        BE -> 
            Debug.todo "rem BE"
        E -> 
            E
        -- 0 children
        T R E x E -> 
            if n == x then 
                E -- EDIT changed line (original: BE) 
            else 
                t
        T B E x E -> 
            if n == x then 
                BE -- EDIT changed line (original: T BB E x E)
            else 
                t
        -- 1 child
        T B (T R E x E) y E -> 
            if n == y then 
                T B E x E 
            else 
                T B (rem n (T R E x E)) y E -- EDIT changed line (original: t)
        T B E y (T R E z E) -> 
            if n == y then 
                T B E z E 
            else 
                T B E y (rem n (T R E z E)) -- EDIT changed line (original: t)
        T _ E _ _ -> 
            Debug.todo "rem"
        T _ _ _  E -> 
            Debug.todo "rem"
        -- 2 children
        T c l y r ->
            if n < y then 
                balance c (bubble_BE_and_BB (rem n l)) y r
            else if n > y then 
                balance c l y (bubble_BE_and_BB (rem n r))
            else -- n == y
                rem_2_children c l y r 
                |> bubble_BE_and_BB -- EDIT added line

rem_2_children : Color -> Tree comparable -> comparable -> Tree comparable -> Tree comparable
rem_2_children c left y right =
    let
        remove_max t =
            case t of
                T R E x E -> -- cases 1a/1b
                    (x, E) 
                T B E x E -> -- cases 2a/2b 
                    (x, BE) 
                T B (T R E w E) x E -> -- cases 3a/3b
                    (x, T B E w E)  
                T color l v r ->
                    let 
                        (x, r2) = remove_max r 
                        rval = T color l v r2 
                               |> bubble_BE_and_BB -- EDIT added lines
                    in
                        (x, rval)
                _ -> 
                    Debug.todo "rem_2_children"
    in
        let 
            (x, new_left) = remove_max left 
        in
            T c new_left x right

orMaybe : Maybe a -> Maybe a -> Maybe a
orMaybe mx my =
    case mx of
        Just _  -> 
            mx
        Nothing -> 
            my

bso t =
    let
        nonDecreasing xs =
            case xs of
                x1::x2::rest -> 
                    x1 <= x2 && nonDecreasing (x2::rest)
                _ -> 
                    True
    in
        toList t |> nonDecreasing

toList : Tree a -> List a
toList t =
    case t of
        E ->
            []
        T _ left x right ->
            toList left ++ [x] ++ toList right
        BE ->
            Debug.todo "BE toList"

maybeBlackHeight t =
    case t of
        E ->
            Just 0
        T c l _ r ->
            maybeBlackHeight l |> Maybe.andThen (\n ->
            maybeBlackHeight r |> Maybe.andThen (\m ->
            if n /= m then 
                Nothing
            else if c == B then 
                Just (1 + n)
            else 
                Just n
            ))
        BE ->
            Debug.todo "BE maybeBlackHeight"

okBlackHeight t =
    case maybeBlackHeight t of
        Just _  -> 
            True
        Nothing -> 
            False

noRedRed t =
    case t of
        E -> 
            True
        T R (T R _ _ _) _ _ -> 
            False
        T R _ _ (T R _ _ _) -> 
            False
        T _ l _ r -> 
            noRedRed l && noRedRed r
        BE ->
            Debug.todo "BE noRedRed"

-- Return true if t is a red-black tree
-- that satisfies invariants; return false otherwise.
redBlackTree t =
    bso t && noRedRed t && okBlackHeight t