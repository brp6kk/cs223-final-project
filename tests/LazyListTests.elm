module LazyListTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import LazyList exposing (..)

suite : Test
suite = 
    describe "Lazy lists"
        [ describe "Range"
            [ test "range 1 5" <|
                \_ ->
                    range 1 5
                    |> toList
                    |> Expect.equal (List.range 1 5) 
            ]
        , describe "Infinite and take"
            [ Test.fuzz (Fuzz.intRange 10 1000) "take from infinite list" <|
                \n ->
                    infinite 0
                    |> take n
                    |> toList
                    |> List.length
                    |> Expect.equal n
            ]
        , describe "Drop"
            [ test "drop from range" <|
                \_ ->
                    range 1 100
                    |> drop 50
                    |> toList
                    |> Expect.equal (List.range 51 100)
            ]
        , describe "Append"
            [ test "append two ranges" <|
                \_ ->
                    append (range 1 10) (range 11 20)
                    |> toList
                    |> Expect.equal (List.range 1 20)
            ]
        ]