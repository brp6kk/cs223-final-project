Eric Y Chang - ericyc16
Partner: None

------------------------------
Proposal 01: Snake

In this project, I will implement a working version of the classic snake video game (https://en.wikipedia.org/wiki/Snake_(video_game_genre)). The user controls the head of a snake, trying to "eat" (collide with) randomly-placed items. On each item consumed, the length of the snake increases by one. The game is over when the user collides with the tail of the snake or the border of the screen. A score will be saved, counting the number of items eaten before game over. The score may be saved only in the browser session or it may be propogated to all application users. This game will have an attractive and responsive UI, and the user can pause the game at any time.

My current experience with the basics of functional programming, the basics of web programming, and randomness will all be useful in implementing this idea. Figuring out some more advanced web programming, especially in terms of animating graphics, will require some additional knowledge.

I'm currently thinking that my model for the snake will require two different data structures. A FIFO queue will be used to update in which cells the snake occupies. When the snake moves forward one space, a new cell will be added to the queue and the oldest cell will be removed from the queue. When the snake eats an item, the next movement will add a new cell to the queue without removing the oldest cell. Implementing such a queue, which allows efficient access to the last element, will require additional techniques.

While this queue makes updating the snake's position easy, searching to determine if the snake has collided with itself will be horribly inefficient. Therefore, a red-black tree holding the cells in which the snake is occupying will also be included with the snake model. Maintaining two data structures that hold the same information may not be the most memory-efficient, but I believe that this is justifiable given the runtime benefits. By the end of week 5 (when this planning is due), I should have the techniques to use red-black trees.

I do not believe that other aspects of my game will be quite as advanced as the model for the snake, and that they can be implemented simply using my current Elm experience.

My current plan of attack is as follows:
1. Get the basic webpage up, prompting the user to press a key to start (but not actually starting a game yet)
   - Getting graphics finalized first will make testing the actual game easier
2. Build the snake model, using Elm Test to ensure that it works
3. Update MVC to allow playthrough of a basic game
   - No pausing, scorekeeping, or replaying yet
4. Update MVC to allow additional game features
5. If there's time, update MVC to save scores (either in browser session or via Firebase; haven't decided which yet)
* Note that the order of steps 1 and 2 are arbitrary; I may end up doing step 2 first, depending on how I'm feeling
------------------------------
Proposal 02: Yukon Solitaire

In this project, I will implement a working version of the Yukon variant of solitaire (https://en.wikipedia.org/wiki/Yukon_(solitaire)). The user manipulates the layout of cards to sort them by suite in order by number. The number of moves made will be logged, with this number being saved. This game will have an attractive and responsive UI, and the user can start a new game or undo a move at any time.

My current experience with the basics of functional programming, the basics of web programming, and randomness will all be useful in implementing this idea. Figuring out some more advanced web programming, especially in terms of animating the movement of cards, will require some additional knowledge.

I'm currently thinking that my model for the tabletop of dealt cards will be a list. Each element of the list will contain a LIFO queue of non-visible cards and a list of visible cards. Off of the tabletop, a list of four LIFO queues will hold each in-order suite. The LIFO queue should be easy to implement - I could just wrap an Elm list, and define enqueuing as adding an element to the front and dequeuing as removing the head. The lists will be more difficult, since they require fast access to a random element, and therefore will require additional techniques.

Randomizing the initial tabletop will require randomness and another random access list. I'm thinking that indices of a list of in-order cards will be randomly generated, with each subsequent generated index representing the next card to place on the board. Again, implementing a fast random-access list will require additional techniques.

My current plan of attack is as follows:
1. Get the basic webpage up, prompting the user to press a key to start (but not actually starting a game yet)
   - Getting graphics finalized first will make testing the actual game easier
2. Build the card type, along with all containers for cards, using Elm Test to ensure that they all work
3. Update MVC to allow playthrough of a basic game
   - No scorekeeping, undoing, or restarting yet
4. Update MVC to allow additional game features
5. If there's time, update MVC to detect when no other moves are possible, alerting the user to the game's end.
6. If there's time, update MVC to save scores (either in browser session or via Firebase; haven't decided which yet)
* Note that the order of steps 1 and 2 are arbitrary; I may end up doing step 2 first, depending on how I'm feeling