{- Based on the file RealTimeQueue.elm,
   but with a change to dequeue to also return the dequeued element
   (plus some trivial naming/spacing changes)
   https://www.classes.cs.uchicago.edu/archive/2021/winter/22300-1/lectures/RealTimeQueues/index.html
-}
module Queue exposing 
    (Queue, empty, isEmpty, enqueue
    , dequeue, peek, getVal, getQueue
    )

import LazyList exposing 
    (Thunk, lazy, force
    , LazyList, LazyListCell(..)
    )

type Queue a =
    Q { front : LazyList a
      , back : List a 
      , schedule : LazyList a
      }

-- Return empty queue
empty : Queue a
empty =
    Q { front = lazy <| \() -> Nil
      , back = []
      , schedule = lazy <| \() -> Nil
      }

-- Determine if queue is empty
isEmpty : Queue a -> Bool
isEmpty (Q {front}) = 
    force front == Nil

-- Adds element to end of queue
enqueue: a -> Queue a -> Queue a
enqueue x (Q {front, back, schedule}) =
    exec front (x::back) schedule

-- Get element at beginning of queue
peek : Queue a -> Maybe a
peek q =
    dequeue q
    |> getVal
-- dequeue returns dequeued element,
-- so take advantage of that

-- Remove element from beginning of queue,
-- returning the element & the modified queue
dequeue : Queue a -> (Maybe a, Queue a)
dequeue (Q {front, back, schedule}) =
    case force front of
        Nil ->
            (Nothing, Q {front = front, back = back, schedule = schedule})
        Cons x frontRest ->
            (Just x, exec frontRest back schedule)
-- Note: unlike RealTimeQueue.elm, I want dequeue to both
-- return the dequeued element and the new queue

-- helper to use with dequeue to just get the dequeued element
getVal : (Maybe a, Queue a) -> Maybe a
getVal (x, _) =
    x

-- helper to use with dequeue to just get the queue
getQueue : (Maybe a, Queue a) -> Queue a
getQueue (_, q) =   
    q

-- perform one step of a combined append and reverse
appendFrontRevBack : LazyList a -> List a -> LazyList a
appendFrontRevBack front back =
    let
        rotate : LazyList a -> List a -> LazyList a -> LazyList a
        rotate lxs ys acc =
            case (force lxs, ys) of
                (Nil, y::[]) ->
                    lazy <| \_ -> Cons y acc
                (Cons x lxRest, y::yRest) ->
                    lazy <| \_ -> Cons x (rotate lxRest yRest (lazy <| \_ -> Cons y acc))
                _ ->
                    Debug.todo "not possible"
    in
        rotate front back <| lazy <| \() -> Nil
                    
-- execute next operation in schedule
exec front back schedule =
    case force schedule of
        Nil ->
            let
                newFront = appendFrontRevBack front back
            in 
                Q { front = newFront, back = [], schedule = newFront }
        Cons _ sRest ->
            Q { front = front, back = back, schedule = sRest }