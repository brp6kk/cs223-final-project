module QueueTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import Queue exposing (..)
import LazyList exposing (..)

queue1 : Queue Int
queue1 =
    List.range 1 10
    |> List.foldl enqueue empty

suite : Test
suite =
    describe "Queue"
    [ describe "isEmpty"
        [ test "empty isEmpty" <|
            \_ -> 
                isEmpty empty
                |> Expect.true "Empty queue should be empty"
        ]
    , describe "enqueue"
        [ test "enqueue 1" <|
            \_ ->
                empty |> enqueue 1 |> peek
                |> Expect.equal (Just 1)
        ]
    , describe "peek"
        [ test "peek empty" <|
            \_ ->
                empty |> peek
                |> Expect.equal Nothing
        , test "peek has elements" <|
            \_ ->
                queue1 |> peek
                |> Expect.equal (Just 1)
        ]
    , describe "dequeue"
        [ test "dequeue empty" <|
            \_ ->
                case dequeue empty of
                    (Nothing, q) ->
                        Expect.true "nonempty queue" (isEmpty q)
                    _ ->
                        Expect.fail "bad dequeue on empty queue"
        , test "dequeue nonempty once" <|
            \_ ->
                queue1 |> dequeue |> getVal
                |> Expect.equal (Just 1)
        , test "dequeue nonempty twice" <|
            \_ ->
                queue1 |> dequeue |> getQueue |> dequeue |> getVal
                |> Expect.equal (Just 2)
        , test "dequeue single elem queue multiple times" <|
            \_ ->
                let
                    result = empty |> enqueue 1 |> dequeue |> getQueue |> dequeue
                in
                    case result of
                        (Nothing, q) ->
                            Expect.true "nonempty queue" (isEmpty q)
                        _ ->
                            Expect.fail "bad dequeue after multiple dequeues"
        ]
    ]