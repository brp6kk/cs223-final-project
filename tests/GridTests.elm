module GridTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import Grid exposing (..)

grid1 : Grid
grid1 = 
    makeGrid 10 10

cell1 : Cell
cell1 = 
    case makeCell grid1 45 of 
        Just c -> 
            c
        _ ->
            Debug.todo "impossible"

cell2 : Cell
cell2 =
    case makeCell grid1 0 of
        Just c ->
            c
        _ ->
            Debug.todo "impossible"

cell3 : Cell
cell3 =
    case makeCell grid1 99 of
        Just c ->
            c
        _ -> 
            Debug.todo "impossible"

suite : Test
suite = 
    describe "Grid"
        [ describe "makeGrid, width, & height" 
            [ test "make square grid with width/height = 3" <|
                \_ ->
                    let
                        g = makeGrid 3 3
                    in
                        width g == 3 && height g == 3
                        |> Expect.true "make failure"
            , test "negative input to makeGrid" <|
                \_ ->
                    let
                        g = makeGrid -1 -1
                    in
                        width g == 0 && height g == 0
                        |> Expect.true "make failure with negatives"
            , test "one negative input to makeGrid" <|
                \_ ->
                    let
                        g = makeGrid 1 -1
                    in
                        width g == 0 && height g == 0
                        |> Expect.true "make failure with one negative"
            ]
        , describe "size"
            [ test "size on valid grid" <|
                \_ -> 
                    makeGrid 20 10
                    |> size
                    |> Expect.equal (20 * 10)
            , test "size on negative grid" <|
                \_ -> 
                    makeGrid -1 -1
                    |> size
                    |> Expect.equal 0
            ]
        , describe "makeCell"
            [ test "make valid cell" <|
                \_ -> 
                    makeCell grid1 50
                    |> Expect.equal (Just 50)
            , test "make too large cell" <|
                \_ -> 
                    makeCell grid1 10000
                    |> Expect.equal Nothing
            , test "make negative cell" <|
                \_ -> 
                    makeCell grid1 -1
                    |> Expect.equal Nothing
            ]
        , describe "cellRow"
            [ test "cellRow row 0" <|
                \_ -> 
                    case makeCell grid1 4 of
                        Just c -> 
                            cellRow grid1 c
                            |> Expect.equal 0
                        _ -> 
                            Expect.fail "Nothing"
            , test "cellRow last row" <|
                \_ -> 
                    case makeCell grid1 97 of 
                        Just c ->
                            cellRow grid1 c
                            |> Expect.equal 9
                        _ -> 
                            Expect.fail "Nothing"
            , test "cellRow middle row" <|
                \_ -> 
                    case makeCell grid1 34 of
                        Just c -> 
                            cellRow grid1 c
                            |> Expect.equal 3
                        _ -> 
                            Expect.fail "Nothing"
            ]
        , describe "cellColumn"
            [ test "cellColumn column 0" <|
                \_ -> 
                    case makeCell grid1 10 of
                        Just c -> 
                            cellColumn grid1 c
                            |> Expect.equal 0
                        _ ->
                            Expect.fail "Nothing"
            , test "cellColumn last column" <|
                \_ -> 
                    case makeCell grid1 89 of
                        Just c ->
                            cellColumn grid1 c
                            |> Expect.equal 9
                        _ -> 
                            Expect.fail "Nothing"
            , test "cellColumn middle column" <|
                \_ -> 
                    case makeCell grid1 57 of
                        Just c -> 
                            cellColumn grid1 c
                            |> Expect.equal 7
                        _ -> 
                            Expect.fail "Nothing"
            ]
        , describe "nextCell"
            [ test "nextCell move north valid" <|
                \_ ->
                    nextCell grid1 cell1 North
                    |> Expect.equal (Just 35)
            , test "nextCell move south valid" <|
                \_ -> 
                    nextCell grid1 cell1 South
                    |> Expect.equal (Just 55)
            , test "nextCell move east valid" <|
                \_ -> 
                    nextCell grid1 cell1 East
                    |> Expect.equal (Just 46)
            , test "nextCell move west valid" <|
                \_ -> 
                    nextCell grid1 cell1 West
                    |> Expect.equal (Just 44)
            , test "nextCell move north bad" <|
                \_ -> 
                    nextCell grid1 cell2 North
                    |> Expect.equal Nothing
            , test "nextCell move south bad" <|
                \_ ->
                    nextCell grid1 cell3 South
                    |> Expect.equal Nothing
            , test "nextCell move east bad" <|
                \_ ->
                    nextCell grid1 cell3 East
                    |> Expect.equal Nothing
            , test "nextCell move west bad" <|
                \_ -> 
                    nextCell grid1 cell2 West
                    |> Expect.equal Nothing
            ]
    ]