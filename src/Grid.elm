module Grid exposing (..)

-- A grid is defined by its max width and max height
-- Grid (width) (height)
type Grid
    = Grid Int Int

-- Cells are meaningless in the abscence of an associated grid.
-- This grid is used to determine if the cell exists as part of the grid.
type alias Cell
    = Int

-- Movement direction.
type Direction
    = North
    | South
    | East
    | West

-- Create a grid given a width and height.
-- On negative input, a grid with 0 rows and columns 
-- is returned instead.
makeGrid : Int -> Int -> Grid
makeGrid w h = 
    if w < 0 || h < 0 then
        Grid 0 0
    else
        Grid w h

-- Get the width of a grid.
width : Grid -> Int
width (Grid w _) =
    w

-- Get the height of a grid.
height : Grid -> Int
height (Grid _ h) =
    h

-- Get the total number of cells within a grid.
size : Grid -> Int
size (Grid w h) =
    w * h

-- Create a cell that is part of a grid,
-- or return nothing if such a cell
-- does not exist within the grid.
makeCell : Grid -> Int -> Maybe Cell
makeCell g x =
    if x < 0 || x >= size g then
        Nothing
    else 
        Just x

-- Determine the row in a grid
-- that a cell occupies.
cellRow : Grid -> Cell -> Int
cellRow g c = 
    c // width g

-- Determine the column in a grid
-- that a cell occupies
cellColumn : Grid -> Cell -> Int
cellColumn g c =
    modBy (width g) c

-- Get the cell in the grid that exists when 
-- moving 1 space from a previous cell in a direction,
-- or nothing if this movement goes off of the grid.
nextCell : Grid -> Cell -> Direction -> Maybe Cell
nextCell g c d = 
    let
        newCell =
            case d of
                North -> c - width g
                South -> c + width g
                East -> c + 1
                West -> c - 1
        newCellRow = cellRow g newCell
        oldCellRow = cellRow g c
    in
        -- moved off of grid.
        -- part of this involves check if a movement east/west 
        -- resulted in moving to a new row.
        if newCell < 0 || newCell > size g || 
                ((d == East || d == West) && 
                 newCellRow /= oldCellRow) then
            Nothing
        else
            Just newCell